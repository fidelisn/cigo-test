<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    // get home page
    public function getOrdersPage(){

        return view('orders');
    }

    //store order
    public function storeOrder(Request $request){

        $validatedOrderData = $request->validate([
            'firstName'=>'required|max:20',
            'lastName'=>'required|max:20',
            'phoneNumber'=>'required|numeric|digits_between:8,11',
            'schedulesDate' =>'required|date|after_or_equal:today',
            'streetAddress'=>'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'email'=>'email',
            'postalCode'=>'required',

        ]);

        $defaultStatus = 'pending';

        $order = new Order; //create instance of order model

        $order->firstname = $request->input('firstName');
        $order->lastname = $request->input('lastName');
        $order->email = $request->input('email');
        $order->phone = $request->input('phoneNumber');
        $order->scheduled_date = $request->input('schedulesDate');
        $order->street_address = $request->input('streetAddress');
        $order->city = $request->input('city');
        $order->state = $request->input('state');
        $order->postal_code = $request->input('postalCode');
        $order->status = $defaultStatus;
        $order->country = $request->input('country');

        $order->save();  // save order information in database

        return redirect()->route('get.orders')
                         ->with('success', 'Your order has been created. You can scroll down to see the order');
    }



    public function getOrders()
    {

        $orders = Order::orderBy('created_at','desc')->paginate(13);

        return view('orders')->with('orders', $orders);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteOrder($id)
    {
        //
        $order = Order::find($id);
        $order->delete();

        return redirect('/')->with('success', 'order deleted!');
    }

    public function changeOrderStatus(Request $request,$id,$status)
    {

          Order::where('id', $id)->update(['status' => $status]);

          return redirect('/')->with('success', 'Order status has been changed');

    }

}
