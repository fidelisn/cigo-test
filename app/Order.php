<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname', 'email', 'phone', 'scheduled_date', 'street_address', 'city', 'state', 'postal_code',
         'status','country',
    ];

    //table name
    protected $table ='orders';

    //primary key
    public $primaryKey ='id';



}
