# README #

This project is a Cigo coding assestment

### What was used to build the site(stack) ###

* Laravel 5.8
* PHP 7.2
* MySQL
* Javascript

### How do I get set up? ###

* Clone the repository on local machine or server(git clone https://fidelisn@bitbucket.org/fidelisn/cigo-test.git)

* Normally the next step will be to run composer install in for the external modules to be created and stored in a vendor folder. But for simplicity, the vendor folder was
not gitignored so it should exist in the project. The only thing to do at this point is to double check that the vendor folder exist

* Create a .env file; for security purposes the .env should not be shared in the repository but for the sake of simplicity, the .env is shared in this case
 so double check if you have it in the project root. if get one by copying  the contents of .env.example to .env (cp .env.example .env) 

* Generate an app key by running this command :  php artisan key:generate

* Create a database (use the database credentials in the .env file or change the credentials in the .env file to match yours)

* Import the SQL file that was provided into your database in order to get the database tables

* You should be able to see the app at this point. If you are setting up locally you can do : php artisan serve 
and a link to view the site will be provided

* The app should be able to work at this point. Good luck !!

### Who do I talk to? ###

* Repo owner or admin : Fidelis Njikem (feeln10@yahoo.ca)

