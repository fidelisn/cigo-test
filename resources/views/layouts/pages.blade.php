<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cigo | Orders </title>

    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--Boostrap version 4.2.1  CDN -->
    @include('partials._stylesheets')

</head>
<body style="background-color:#eeeeee">
<div id="app">
@include('partials._navbar')
@include('partials._messages')
@yield('content')





<!-- Boostrap JS dependcies-->
@include('partials._scripts')

@yield('scripts') <!-- page specific script -->
</div>
</body>
</html>
