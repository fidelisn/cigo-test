@extends('layouts.pages')

@section('content')



        <div class="col-sm-8 offset-sm-2 mt-3" style="background-color: white;">
            <div>
            <h3 class="text-left mt-3 text-secondary">Add an Order
                <div class="text-right text-primary"> <i class="fas fa-user"></i> </div>
                </h3>
            </div>
            <hr>


            <form action="{{ route('store.order') }}" method="POST" id="orderForm">
                @csrf
                <div class="form-row">

                    <div class="form-group col-md-6">
                        <label>First Name <a class="text-danger">*</a> </label>
                        <input type="text" name="firstName" class="form-control {{ $errors->has('firstName') ? ' is-invalid' : '' }}"
                               value="{{ old('firstName') }}" placeholder="First Name">
                        @if ($errors->has('firstName'))
                            <span class="invalid-feedback" role="alert">
                               <strong>{{ $errors->first('firstName') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div class="form-group col-md-6">
                        <label>Last name</label>
                        <input type="text"  name="lastName" class="form-control {{ $errors->has('lastName') ? ' is-invalid' : '' }}"
                               value="{{ old('lastName') }}" id="first_name" placeholder="Last Name" >
                        @if ($errors->has('lastName'))
                            <span class="invalid-feedback" role="alert">
                               <strong>{{ $errors->first('lastName') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div class="form-group col-md-6">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                               value="{{ old('email') }}"  id="inputlastname" placeholder="you@sample.com">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                               <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div class="form-group col-md-6">
                        <label>Phone Number <a class="text-danger">*</a> </label>
                        <input type="tel" name="phoneNumber" class="form-control {{ $errors->has('phoneNumber') ? ' is-invalid' : '' }}"
                               value="{{old('phoneNumber')}}"  placeholder="+1 (888) 123-4567">
                        @if ($errors->has('phoneNumber'))
                            <span class="invalid-feedback" role="alert">
                               <strong>{{ $errors->first('phoneNumber') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="col-md-6">
                        <label> Schedules Date  <a class="text-danger">*</a> </label>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i> </span>
                            </div>

                            <input type="text"  name="schedulesDate" id="datepickerSchedule"
                                   data-date-format="yyyy-mm-dd" value="{{old('schedulesDate')}}"
                                   placeholder=""
                                   class="form-control {{ $errors->has('schedulesDate') ? ' is-invalid' : '' }}" />

                            @if ($errors->has('schedulesDate'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('schedulesDate') }}</strong>
                           </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group col-md-12 mt-2" id="locationField">
                        <label>Street Address <a class="text-danger">*</a> </label>
                        <input type="text" name="streetAddress" id="autocomplete" class="form-control {{ $errors->has('streetAddress') ? ' is-invalid' : '' }}"
                               value="{{old('streetAddress')}}"  placeholder="" onFocus="geolocate()">

                        @if ($errors->has('streetAddress'))
                            <span class="invalid-feedback" role="alert">
                               <strong>{{ $errors->first('streetAddress') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div class="form-group col-md-6">
                        <label>City <a class="text-danger">*</a> </label>
                        <input type="text" id="locality" class="form-control {{ $errors->has('city') ? ' is-invalid' : '' }} "
                               value="{{old('city')}}" name="city" placeholder="" >
                        @if ($errors->has('city'))
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('city') }}</strong>
                           </span>
                        @endif

                    </div>


                    <div class="form-group col-md-6">
                        <label>State/Province <a class="text-danger">*</a> </label>
                        <input type="text" name="state" class="form-control {{ $errors->has('state') ? ' is-invalid' : '' }}"
                               id="administrative_area_level_1" value="{{old('state')}}" placeholder="">
                        @if ($errors->has('state'))
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('state') }}</strong>
                           </span>
                        @endif

                    </div>


                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Postal / Zip Code</label>
                        <input type="text" name="postalCode" class="form-control {{ $errors->has('postalCode') ? ' is-invalid' : '' }}"
                               id="postal_code" value="{{old('postalCode')}}" placeholder="">
                        @if ($errors->has('postalCode'))
                            <span class="invalid-feedback" role="alert">
                               <strong>{{ $errors->first('postalCode') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div class="form-group col-md-6">
                        <label>Country <a class="text-danger">*</a> </label>
                         <select  name="country"  class="form-control">
                            <option   value="Canada" {{ old('country')=="Canada" ? 'selected' :'' }}>Canada</option>
                            <option   value="United States" {{ old('country')=="United States" ? 'selected' :'' }}>United States</option>
                            <option   value="Mexico" {{ old('country')=="Mexico" ? 'selected' :'' }}>Mexico</option>
                            <option   value="Canada" {{ old('country')=="Columbia" ? 'selected' :'' }}>Columbia</option>
                         </select>

                        @if ($errors->has('country'))
                            <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('country') }}</strong>
                           </span>
                        @endif
                    </div>

                </div>

                    <div class="text-right">
         <button type="button" onclick="resetOrderForm();" class="btn btn-danger"> Cancel</button>
         <button type="submit" class="btn btn-success"> Submit</button>

    </div>


     </form>

 </div>




<div class="col-sm-8 offset-sm-2 mt-3" style="background-color: white;">

     <div>
         <h3 class="text-left mt-3 text-secondary">Existing Orders
             <div class="text-right text-primary"> <i class="far fa-check-square"></i> </div>
         </h3>
     </div>
         <div class="table-responsive">
             <table class="table">
                 <thead class="thead-dark">

                 <tr>
                     <td><strong>FirstName</strong></td>
                     <td><strong>LastName</strong></td>
                     <td><strong>Date</strong></td>
                     <td></td>

                 </tr>

                 </thead>

                 <tbody>


                 @foreach($orders as $order)
                     <tr>
                         <td>{{$order->firstname}}</td>
                         <td>{{$order->lastname}}</td>
                         <td>{{$order->scheduled_date}}</td>

                         <td class="text-right">

                             @switch( $order->status)

                                 @case( "assigned" )
                                   @include('partials._assigned')
                                 @break

                                 @case( 'cancelled' )
                                   @include('partials._cancelled')
                                 @break

                                 @case( 'onroute' )
                                   @include('partials._onroute')
                                 @break

                                 @case( 'done' )
                                   @include('partials._done')
                                 @break

                                 @default
                                    @include('partials._pending')
                             @endswitch

                         </td>

                     </tr>
                 @endforeach

                 </tbody>
             </table>

             <div class="float-right">
                {{$orders->links()}} <!-- print pagination links -->
             </div>

         </div>
         <div>
         </div>




     </div>



@endsection

<!--page specific scripts (script for datepicker) -->
@section('scripts')
<script>

 //date picker
 $('#datepickerSchedule').datepicker({
     weekStart: 1,
     daysOfWeekHighlighted: "6,0",
     autoclose: true,
     todayHighlight: true,
 });
 $('#datepickerSchedule').datepicker();
 //to load current date in date input field use
 $('#datepickerSchedule').datepicker("setDate", new Date());

 //reset orders form
 function resetOrderForm() {
     document.getElementById("orderForm").reset();
 }



 //google maps
 "use strict";
 // This sample uses the Autocomplete widget to help the user select a
 // place, then it retrieves the address components associated with that
 // place, and then it populates the form fields with those details.
 // This sample requires the Places library. Include the libraries=places
 // parameter when you first load the API. For example:
 // <script
 // src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPJpjD-qcR_yIxJnS8maR5W9KB0E3EzYI&libraries=places">
 let placeSearch;
 let autocomplete;
 const componentForm = {
     //street_number: "short_name",
     //route: "long_name",
     locality: "long_name",
     administrative_area_level_1: "short_name",
     //country: "long_name",
     postal_code: "short_name"
 };

 function initAutocomplete() {
     // Create the autocomplete object, restricting the search predictions to
     // geographical location types.
     autocomplete = new google.maps.places.Autocomplete(
         document.getElementById("autocomplete"),
         {
             types: ["geocode"]
         }
     ); // Avoid paying for data that you don't need by restricting the set of
     // place fields that are returned to just the address components.

     autocomplete.setFields(["address_component"]); // When the user selects an address from the drop-down, populate the
     // address fields in the form.

     autocomplete.addListener("place_changed", fillInAddress);
 }

 function fillInAddress() {
     // Get the place details from the autocomplete object.
     const place = autocomplete.getPlace();

     for (const component in componentForm) {
         document.getElementById(component).value = "";
         document.getElementById(component).disabled = false;
     } // Get each component of the address from the place details,
     // and then fill-in the corresponding field on the form.

     for (const component of place.address_components) {
         const addressType = component.types[0];

         if (componentForm[addressType]) {
             const val = component[componentForm[addressType]];
             document.getElementById(addressType).value = val;
         }
     }
 } // Bias the autocomplete object to the user's geographical location,
 // as supplied by the browser's 'navigator.geolocation' object.

 function geolocate() {
     if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(position => {
             const geolocation = {
                 lat: position.coords.latitude,
                 lng: position.coords.longitude
             };
             const circle = new google.maps.Circle({
                 center: geolocation,
                 radius: position.coords.accuracy
             });
             autocomplete.setBounds(circle.getBounds());
         });
     }
 }


</script>


@endsection
