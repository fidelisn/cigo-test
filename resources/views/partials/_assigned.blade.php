<div class="btn-group">
    <button type="button" class="btn btn-primary btn-sm">ASSIGNED</button>
    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
        <span class="caret"></span>
    </button>
    <div class="dropdown-menu">
        <a class="dropdown-item" href="{{ route('order.change', [$order->id,'pending'])}}">Pending</a>
        <a class="dropdown-item" href="{{ route('order.change', [$order->id,'onroute'])}}">On Route</a>
        <a class="dropdown-item" href="{{ route('order.change', [$order->id,'done'])}}">Done</a>
        <a class="dropdown-item" href="{{ route('order.change', [$order->id,'cancelled'])}}">Cancelled</a>
    </div>

    <form action="{{ route('order.delete', $order->id)}}" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-sm" onclick="return confirm('Deleting an order is permanent.' +
                                       ' Are you sure you want to delete ?')" >
            <i class="fas fa-times-circle text-danger"></i> </button>
    </form>

</div>
