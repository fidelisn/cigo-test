
{{-- Success Alert --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Success:</strong> {{ Session::get('success') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

{{-- Danger Alert --}}
@if(Session::has('danger'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Error:</strong> {{ Session::get('danger') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

{{-- Warning Alert --}}
@if(Session::has('warning'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Something Went Wrong!</strong>
        <br>
        {{ Session::get('warning') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

{{-- Informational Alert --}}
@if(Session::has('info'))
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <strong>Heads Up!</strong> {{ Session::get('info') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif



{{-- If the page has any errors passed to it --}}
@if(count($errors) > 0)

    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Errors:</strong>

        <ul>
            @foreach($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach
        </ul>

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

@endif
