<?php

Route::get('/','OrdersController@getOrdersPage')->name('home.page');

Route::post('/','OrdersController@storeOrder')->name('store.order');

Route::get('/', 'OrdersController@getOrders')->name('get.orders');

Route::delete('/order-delete/{id}', 'OrdersController@deleteOrder')->name('order.delete');

Route::get('/order-status-change/{id}/{status}', 'OrdersController@changeOrderStatus')->name('order.change');

